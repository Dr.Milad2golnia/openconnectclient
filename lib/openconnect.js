"use strict";

const debug = require("debug");
var execFile = require('child_process').execFile;
var inpathSync = require('inpath').sync;
var kext = require('kext');
var spawn = require('child_process').spawn;
var sudo = require('sudo');
const util = require('util');
var path = process.env['PATH'].split(':');
const success = debug('app::success');
const fail = debug('app::fail');
const info = debug('app::info');

/**
 * Types
 */
/**
 * @typedef {'DTLS'|'CONNECTED'|'CSTP'} establishmentLevel()
 */

var openconnectBinary = inpathSync('openconnect', path);
const asyncAvailable = util.promisify(available);
const asyncConnect = util.promisify(connect);
const asyncDisconnect = util.promisify(disconnect);
let connectionEstablished = /Established DTLS connection/

exports = module.exports = {
    available: asyncAvailable,
    connect: asyncConnect,
    disconnect: asyncDisconnect,
    establishment:  establishment
};

/**
 * Define what you mean by a successful connection.
 * @param {establishmentLevel} level Indicates what you mean by 
 *  connection establishment. 
 *  1. CONNECTED is first event occurred
 *      and it is when VPN Server assigns you an IP 
 *  2. CSTP is second step.
 *  3. DTLS is final step which means the connection is
 *      fully available.
 */
function establishment(level){
    if(level === 'CSTP'){
        connectionEstablished = 
            /CSTP connected/
    }else if(level === 'DTLS'){
        connectionEstablished = 
            /Established DTLS connection/
    }else if(level === 'CONNECTED'){
        connectionEstablished = 
            /HTTP\/1\.1 200 CONNECTED/
    }
}

function available(callback) {
    function run(err, loaded) {
        if (err) {
            return callback(err);
        }
        if (!loaded) {
            callback(new Error('Tunnel module not loadable'));
        }

        execFile(openconnectBinary, ['--version'], function (err, stdout, stderr) {
            if (err) {
                callback(err);
            } else {
                var v = (stdout + stderr).match(/(OpenConnect version v[0-9.]+)/);
                if (v) {
                    callback(null, { openConnect: openconnectBinary, version: v[1] });
                } else {
                    callback(new Error('Could not parse OpenConnect version string'));
                }
            }
        });
    }

    if (!openconnectBinary) {
        process.nextTick(function () {
            callback(new Error('Could not find vpnc in $PATH'));
        });
    } else {
        if (process.platform === 'darwin')
            kext.ensure(['foo.tun', 'net.tunnelblick.tun', 'com.viscosityvpn.Viscosity.tun'], '/Library/Extensions/tun.kext', run);
        else
            run(null, true);
    }
}

var child;
function connect(config, callback) {
    var args = ['openconnect', '--non-inter', '--passwd-on-stdin'];

    for (var key in config) {
        if (Object.prototype.hasOwnProperty.call(config, key)) {
            if (key === 'password') {
                continue;
            } else if (key === 'server') {
                args.push(config[key]);
            } else if (config[key] === 'yes') {
                args.push('--' + key);
            } else {
                args.push('--' + key + '=' + config[key]);
            }
        }
    }

    info('sudo', args);
    child = sudo(args);
    child.on('started', function () {

        child.stdout.on('data', function (data) {
            data.toString().split('\n').forEach(function logStdout(line) { success(line); });
            if (data.toString().match(/Established DTLS connection/)) {
                success('connected');
                callback(null, 0);
            };
        });

        child.stderr.on('data', function (data) {
            data.toString().split('\n').forEach(function logStderr(line) { info(line); });
            // if (data.toString().match(/Login failed/)) {
            //     console.warn(args.join(' '));
            //     console.warn('openconnect: Login failed');
            //     callback(/* error= */ null, /* exit code = */ 1);
            // }
        });

        child.on('close', (data)=>{
            data.toString().split('\n').forEach(function logStderr(line) { fail(line); });
            fail(args.join(' '));
            fail('openconnect: Login failed');
            callback(/* error= */ new Error(`Error! Unable to connect`));
        })

        child.stdin.write(config.password + '\n');
    });

    return child;
}

function disconnect(callback) {
    con.debug('sudo -s kill ' + child.pid);
    sudo(['-s', 'kill', '' + child.pid], { cachePassword: true }).on('exit', function (code) {
        callback(null, code);
    });
}

