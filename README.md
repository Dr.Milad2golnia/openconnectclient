# Openconnect Client
This projected is forked from [openconnect][mainOC] which is not maintained anymore by its developer.

# Usage
There are developed four simple functions which we describe them respectively.
## `available()`
Check if [`openconnect`][oc] is installed on your system or not, if it is installed it will return the information about version and path to installed program and throws an error otherwise.  
Example:
```
    try{
        let result = await available();
        /*
        *   result = {
        *    openConnect: '/usr/bin/openconnect',
        *   version: 'OpenConnect version v9.01'
        *   }
        */
    }catch(err){
        //Error handling
    }
```

## `connect(config)`
* `config`: an object with following type:

```
    {
        password:       string,
        server:         string,
        otherParam:     value
    }
```

`otherParam` is some other parameters which you wish to pass to [`openconnect`][oc] in commandline. It means `otherParam` is used as following form:
```
    $ openconnect --otherParam = value
```

*NOTE*: You can use `otherParam` to pass username, e.g.  
```
    {
        password:       'secret',
        server:         'xxxxx.com:443',
        user:           'username'
    }
```

## `disconnect()`
Break provious connection and disconnect you from server.  
Example:
```
    try{
        let result = await disconnect();
        /*
        *   result contains `exit code`
        */
    }catch(err){
        //Error handling
    }
```

## `establishment(level)`
* `level`: Level of connection

Using this method you can determine what you mean by a successful connection!  
Usually there are three step to connect to the server:
1. Connection establishment by receiveing `200` status code.
2. `CSTP` connection.
3. `DTLS` connection.

A connection is considered secure when all three above steps are passed but may have a server which don't do all of above step.
In that case you need to change level of establishment and tell this module, when a connection is considered as sucessful.

`level` is one of the following strings: `'CONNECTED'`, `'CSTP'` and `'DTLS'` which are respectively corresponding to mentioned steps.

*NOTE:* Default level is `DTLS` connection which is the most secure state of connection.

# What's new?
There were some problems with original module! e.g. The failure scenario was considered just when you see following message in output:
```
Login failed
```
But we know this is not just the case when we face failure, there are some scenarios when we come across failure and if we don't handle them correctly, it will cause main program to abort.

Another problem with original module is that it recognize successful scenarios just when you see following message in output:
```
Established DTLS connection
```
In some servers, you can connect to the server and get IP address but DTLS connection is not available.  
In this module we provided a way to define what you mean by successful connection. 

Some other minor changes are applied to this project but we don't mention them here.

# Platform

This project is developd based on [`openconnect`][oc] and hence it only works if you are installed this program.

It is tested on `Manjaro` and works correctly but about other platfroms: if `openconnect` is available, this module should work correctly too.

# License
MIT

[mainOC]:https://www.npmjs.com/package/openconnect
[oc]:   https://github.com/openconnect/openconnect